# -*- coding: utf-8 -*-

from odoo import api, fields, models


class SaleOrderInherit(models.Model):
    _inherit = "sale.order"
    
    discount= fields.Float("Discount(%)")


    @api.depends('order_line.price_total','discount')
    def _amount_all(self):
      
  
        for order in self:
            # for line in order.order_line:
            #     line.discount= order.discount
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            discount_amount=amount_untaxed*order.discount/100
            total_discount_amount=amount_untaxed-discount_amount
            order.update({
                'amount_untaxed': total_discount_amount,
                'amount_tax': amount_tax,
                'amount_total': total_discount_amount + amount_tax,
            })


    
    def _create_invoices(self, grouped=False, final=False, date=None):
        res = super(SaleOrderInherit, self)._create_invoices(grouped=grouped, final=final)
        print("ssssssssssssssssssssssssssssssssssssss",res)
        res.write({"discount":self.discount})
        return res
